import numpy as np
from skimage import io
import tifffile

def tifimp(img_path):
    #generate image array
    img_array = io.imread(img_path)

    with tifffile.TiffFile(img_path) as tif:
        #read tags for relevent metadata
        xres = tif.pages[0].tags['XResolution']
        unit = tif.pages[0].tags['ResolutionUnit']
        width = tif.pages[0].tags['ImageWidth']
        height = tif.pages[0].tags['ImageLength']
        
        if unit.value.name == 'CENTIMETER':
            #convert pixels/cm to um/pixel
            scale = (1/(xres.value[0]/xres.value[1]))*10000 
        else:
            #TO DO
            return('XRes in inches. Not supported :(')

    #make a dictionary from all info, with a nested metadata dict to be fed directly to napari viewer
    tiff_dict = {
            'img':img_array,
            'meta':{
                'scale':scale,
                'unit':unit.value.name,
                'width':width.value,
                'height':height.value
                },
            '100nm':np.array([[0,0],[0,0.1/scale]]),
            '10um':np.array([[0,0],[0,10/scale]]),
            '50um':np.array([[0,0],[0,50/scale]]),
            '100um':np.array([[0,0],[0,100/scale]]),
            }

    return(tiff_dict)

#test
